<?php
include_once 'initialise.php';
include_once 'submit.php';;

/**
 * Experimenting with SESSION.
 */
if (session_status() == PHP_SESSION_NONE) session_start();

$pageWasRefreshed = isset($_SERVER['HTTP_CACHE_CONTROL']) && $_SERVER['HTTP_CACHE_CONTROL'] === 'max-age=0';
if ($pageWasRefreshed && !(isset($_SESSION['errors']) || isset($_SESSION['input']))) {
    if(session_status() == PHP_SESSION_ACTIVE ) {
        if (isset($_SESSION['data'])) {
            session_unset();
            session_destroy();
            session_start();
        }
        dataInit();
    } else if (session_status() == PHP_SESSION_NONE) {
        session_start();
        if (!isset($_SESSION['data'])) {
            session_unset();
            session_destroy();
            session_start();
        }
        dataInit();
    } else if (session_status() == PHP_SESSION_DISABLED) {
        echo 'Code BUG: session status DISABLED in refreshed<br/>';
    } else {
        echo 'code BUG: else call in refreshed.<br/>';
    }
} else {
    if(session_status() == PHP_SESSION_ACTIVE ) {
        if (!isset($_SESSION['data'])) {
            echo 'code BUG: data not set.<br/>';
            dataInit();
        }
    } else if (session_status() == PHP_SESSION_NONE) {
        echo 'code BUG: session status NONE in !refreshed<br/>';
        session_start();
        if (!isset($_SESSION['data'])) {
            echo 'code BUG: data not set.<br/>';
            dataInit();
        } else {
            echo "DATA ISSET";
        }
    } else if (session_status() == PHP_SESSION_DISABLED) {
        echo 'code BUG: session status DISABLED in !refreshed<br/>';
    } else {
        echo 'code BUG: else call in !refreshed.<br/>';
    }
}
?>

<!DOCTYPE html>
<html lang="cs">
<head>
    <meta charset="utf-8">
    <title>DU - csv Data</title>
</head>
<body>
<form method="get" action="filter.php">
    <button type="submit" name="jmeno" id="jmeno">Jména</button>
    <button type="submit" name="prijmeni">Příjmení</button>
    <button type="submit" name="ulice">Ulice</button>
    <button type="submit" name="nadrizeny">Nadřízený</button>
    <button type="submit" name="reset">Reset</button>
</form>

<?php
drawTable($_SESSION['data']);
function drawTable($data): void
{
    echo '<table border="1">';
    echo '<tr>';
    echo '<td>key</td>';
    foreach ($_SESSION['columnName'] as $el) {
        echo '<td>'.$el.'</td>';
    }
    echo '</tr>';

    foreach ($data as $el => $radek) {
        echo '<tr>';
        echo '<td>'.$el.'</td>';
        foreach ($radek as $i => $value) {
            echo '<td>'.$value.'</td>';
        }
        echo '</tr>';
    }
    echo '</table>';
}
?>

<form method="post" action="submit.php">
    <?php
    $optionsKeys = array_keys($_SESSION['data']);
    sort($optionsKeys);
    ?>
    <label for="klic">Klíč</label><br/>
    <select id="klic" name="klic">
        <option value="" <?= (isset($_SESSION['inputKlic']) && $_SESSION['inputKlic'] == "") ? 'selected' : '' ?>></option>
        <?php foreach ($optionsKeys as $key) { ?>
            <option value="<?= $key ?>" <?= (isset($_SESSION['inputKlic']) && $_SESSION['inputKlic'] == $key) ? 'selected' : '' ?>><?= $key ?></option>
        <?php } ?>
    </select><br/>
    <button type="submit" name="smazatKlic">Smazat zvolený klíč</button>
    <button type="submit" name="zobrazitKlic">Zobrazit zvolený klíč</button>
    <br/>
    <?php
    if (isset($_SESSION['errors']['keySubmit'])) {
        echo '<div style="color:red;">'.$_SESSION['errors']['keySubmit'].'</div>';
    }
    ?>
    <br/>
    <label for="jmeno">Jméno*</label><br/>
    <input type="text" id="jmeno" name="jmeno" value="<?php echo htmlspecialchars($_SESSION['input']['jmeno'] ?? '')?>"/><br>
    <?php
    if (isset($_SESSION['errors']['jmeno'])) {
        echo '<div style="color:red;">'.$_SESSION['errors']['jmeno'].'</div>';
    }
    ?>

    <label for="prijmeni">Příjmení*</label><br/>
    <input type="text" id="prijmeni" name="prijmeni" value="<?php echo htmlspecialchars($_SESSION['input']['prijmeni'] ?? '')?>"/><br>
    <?php
    if (isset($_SESSION['errors']['prijmeni'])) {
        echo '<div style="color:red;">'.$_SESSION['errors']['prijmeni'].'</div>';
    }
    ?>

    <?php
    $optionsGender = [
        '' => '',
        'M' => 'muž',
        'Ž' => 'žena',
    ];
    ?>
    <label for="pohlavi">Pohlaví</label><br/>
    <select id="pohlavi" name="pohlavi">
        <?php foreach ($optionsGender as $key => $label) { ?>
            <option value="<?= $key ?>" <?= (isset($_SESSION['input']['pohlavi']) && $_SESSION['input']['pohlavi'] == $key) ? 'selected' : '' ?>><?= $label ?></option>
        <?php } ?>
    </select><br/>

    <label for="ulice">Ulice</label><br/>
    <input type="text" id="ulice" name="ulice" value="<?php echo htmlspecialchars($_SESSION['input']['ulice'] ?? '')?>"><br/>
    <?php
    if (isset($_SESSION['errors']['ulice'])) {
        echo '<div style="color:red;">'.$_SESSION['errors']['ulice'].'</div>';
    }
    ?>
    <label for="obec">Obec*</label><br/>
    <input type="text" id="obec" name="obec" value="<?php echo htmlspecialchars($_SESSION['input']['obec'] ?? '')?>"/><br/>
    <?php
    if (isset($_SESSION['errors']['obec'])) {
        echo '<div style="color:red;">'.$_SESSION['errors']['obec'].'</div>';
    }
    ?>

    <label for="psc">PSC*</label><br/>
    <input type="text" id="psc" name="PSC" value="<?php echo htmlspecialchars($_SESSION['input']['PSC'] ?? '')?>"/><br/>
    <?php
    if (isset($_SESSION['errors']['PSC'])) {
        echo '<div style="color:red;">'.$_SESSION['errors']['PSC'].'</div>';
    }
    ?>

    <label for="telefon">Telefon</label><br/>
    <input type="tel" id="telefon" name="telefon" value="<?php echo htmlspecialchars($_SESSION['input']['telefon'] ?? '')?>"/><br/>

    <label for="email">E-mail</label><br/>
    <input type="email" id="email" name="email" value="<?php echo htmlspecialchars($_SESSION['input']['email'] ?? '')?>"/><br/>
    <?php
    if (isset($_SESSION['errors']['email'])) {
        echo '<div style="color:red;">'.$_SESSION['errors']['email'].'</div>';
    }
    ?>

    <label for="zamestnani">Zamestnani</label><br/>
    <input type="text" id="zamestnani" name="zamestnani" value="<?php echo htmlspecialchars($_SESSION['input']['zamestnani'] ?? '')?>"/><br/>

    <?php
    $optionsLeader = [''];
    $optionsLeader = array_merge($optionsLeader, array_values(array_map(function($el){
        if ($el[$_SESSION['columnName'][8]] == 'mistr')
            return $el[$_SESSION['columnName'][0]].' '.$el[$_SESSION['columnName'][1]];
        return $el[$_SESSION['columnName'][9]];}, $_SESSION['data'])));
    $optionsLeader = array_unique($optionsLeader);
    sort($optionsLeader);
    ?>
    <label for="nadrizeny">Nadrizeny</label><br/>
    <select id="nadrizeny" name="nadrizenySelect">
        <?php foreach ($optionsLeader as $val) { ?>
            <option value="<?= $val ?>" <?= (isset($_SESSION['input']['nadrizenySelect']) && $_SESSION['input']['nadrizenySelect'] == $val) ? 'selected' : '' ?>><?= $val ?></option>
        <?php } ?>
    </select>
    <input type="text" id="nadrizeny" name="nadrizenyInput" value="<?php echo htmlspecialchars($_SESSION['input']['nadrizeny'] ?? '')?>"/><br/>
    <?php
    if (isset($_SESSION['errors']['nadrizeny'])) {
        echo '<div style="color:red;">'.$_SESSION['errors']['nadrizeny'].'</div>';
    }
    ?>

    <button type="submit" name="pridat">Odeslat</button>
    <button type="submit" name="upravit">Upravit zvolený klíč</button>
    <?php
    if (isset($_SESSION['errors']['dataSubmit'])) {
        echo '<div style="color:red;">'.$_SESSION['errors']['dataSubmit'].'</div>';
    }
    ?>

    <div>* - povinne udaje</div>
</form>
</body>
</html>

<?php
if (isset($_SESSION['inputKlic'])) {
    $key = $_SESSION['inputKlic'];
    submitSessionUnset();
    $_SESSION['inputKlic'] = $key;
} else {
    submitSessionUnset();
}
?>