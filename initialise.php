<?php

use JetBrains\PhpStorm\NoReturn;

#[NoReturn] function dataInit(): void
{
    if (!isset($_SESSION['data'])) {
        $_SESSION['data'] = init('adresar.csv');
    }
}

function init($filename): array
{
    $file = fopen($filename, 'r');
    $_SESSION['key'] = -1;
    $data = [];

    while (!feof($file)) {

        $radek = fgetcsv($file, 0, ';');

        if ($radek && $_SESSION['key'] === -1) {
            foreach ($radek as $el) {
                $_SESSION['columnName'][] = $el;
            }
            $_SESSION['key']++;
            continue;
        }

        if ($radek) {
            foreach ($radek as $index => $el) {
                $data[$_SESSION['key']][$_SESSION['columnName'][$index]] = $el;
            }
            $_SESSION['key']++;
        }
    }

    fclose($file);
    return $data;
}