<?php

use JetBrains\PhpStorm\NoReturn;

if (session_status() == PHP_SESSION_NONE) session_start();

$filename = 'adresar.csv';

if (!empty($_POST)) {
    if ($_POST['klic'] == '') {
        submitSessionUnset();
        if (isset($_POST['smazatKlic'])) {
            $_SESSION['errors']['keySubmit'] = 'Prosím zvolte klíč ke smazání.';
            fillInput();
            updateUnsuccessful();
        } else if (isset($_POST['zobrazitKlic'])) {
            $_SESSION['errors']['keySubmit'] = 'Prosím zvolte klíč k zobrazení.';
            fillInput();
            updateUnsuccessful();
        } else if (isset($_POST['upravit'])) {
            $_SESSION['errors']['dataSubmit'] = 'Prosím zvolte klíč pro úpravu.';
            fillInput();
            updateUnsuccessful();
        }
    }
    
    if ((isset($_SESSION['inputKlic']) || isset($_POST['klic'])) && isset($_POST['zobrazitKlic'])) {
        if ($_POST['klic'] !== '' || isset($_SESSION['inputKlic'])) {
            if (!isset($_SESSION['inputKlic'])) {
                $_SESSION['inputKlic']= $_POST['klic'];
            } else {
                $_POST['klic'] = $_SESSION['inputKlic'];
            }
            $_SESSION['input']['jmeno'] = $_SESSION['data'][$_POST['klic']][$_SESSION['columnName'][0]];
            $_SESSION['input']['prijmeni'] = $_SESSION['data'][$_POST['klic']][$_SESSION['columnName'][1]];
            $_SESSION['input']['pohlavi'] = $_SESSION['data'][$_POST['klic']][$_SESSION['columnName'][2]];
            $_SESSION['input']['ulice'] = $_SESSION['data'][$_POST['klic']][$_SESSION['columnName'][3]];
            $_SESSION['input']['obec'] = $_SESSION['data'][$_POST['klic']][$_SESSION['columnName'][4]];
            $_SESSION['input']['PSC'] = $_SESSION['data'][$_POST['klic']][$_SESSION['columnName'][5]];
            $_SESSION['input']['telefon'] = $_SESSION['data'][$_POST['klic']][$_SESSION['columnName'][6]];
            $_SESSION['input']['email'] = $_SESSION['data'][$_POST['klic']][$_SESSION['columnName'][7]];
            $_SESSION['input']['zamestnani'] = $_SESSION['data'][$_POST['klic']][$_SESSION['columnName'][8]];
            if ($_SESSION['data'][$_POST['klic']][$_SESSION['columnName'][9]] !== '') {
                $_SESSION['input']['nadrizenySelect'] = $_SESSION['data'][$_POST['klic']][$_SESSION['columnName'][9]];
            }
        } else {
            echo "Code BUG: else call in zobrazit klic";
            exit();
        }
        updateUnsuccessful(); //exception, its successful, but cant use updateSuccessful bcs of unset($_SESSION).
    } else if ((isset($_SESSION['inputKlic']) || isset($_POST['klic'])) && isset($_POST['smazatKlic'])) {
        deleteRow();
        updateSuccessful();
    } else if ((isset($_SESSION['inputKlic']) || isset($_POST['klic'])) && isset($_POST['upravit'])) {
        checkInput();
        if (isset($_SESSION['errors'])) {
            if (isset($_POST['klic'])) {
                $_SESSION['inputKlic'] = $_POST['klic'];
            }
            updateUnsuccessful();
        } else {
            editRow();
            updateSuccessful();
        }
    } else if (isset($_POST['pridat'])) {
        checkInput();
        if (empty($_SESSION['errors'])) {
            $vystup = [];

            $vystup[] = $_POST['jmeno'];
            $vystup[] = $_POST['prijmeni'];
            $vystup[] = $_POST['pohlavi'];
            $vystup[] = $_POST['ulice'];
            $vystup[] = $_POST['obec'];
            $vystup[] = $_POST['PSC'];
            $vystup[] = $_POST['telefon'];
            $vystup[] = $_POST['email'];
            $vystup[] = $_POST['zamestnani'];
            if ($_POST['nadrizenySelect'] !== '' && $_POST['nadrizenyInput'] === '') {
                $vystup[] = $_POST['nadrizenySelect'];
            } else {
                $vystup[] = $_POST['nadrizenyInput'];
            }

            $file = fopen('adresar.csv', 'a');
            fputcsv($file, $vystup, ';');
            fclose($file);

            foreach ($vystup as $index => $el) {
                $_SESSION['data'][$_SESSION['key']][$_SESSION['columnName'][$index]] = $el;
            }
            $_SESSION['key']++;

            updateSuccessful();
        } else {
            updateUnsuccessful();
        }
    } else {
        echo 'code BUG: !empty($_POST) else call.';
        exit();
    }
} else {
    return;
}

function deleteRow(): void
{
    if ($_POST['klic'] != '') {
        unset($_SESSION['data'][$_POST['klic']]);
    } else if (isset($_SESSION['inputKlic'])) {
        unset($_SESSION['data'][$_SESSION['inputKlic']]);
    }

    checkFile();
}

function editRow(): void
{
    if (isset($_POST['klic'])) {
        $_SESSION['inputKlic'] = $_POST['klic'];
    }
    $_SESSION['data'][$_SESSION['inputKlic']][$_SESSION['columnName'][0]] = clean_text($_POST['jmeno']);
    $_SESSION['data'][$_SESSION['inputKlic']][$_SESSION['columnName'][1]] = clean_text($_POST['prijmeni']);
    $_SESSION['data'][$_SESSION['inputKlic']][$_SESSION['columnName'][2]] = clean_text($_POST['pohlavi']);
    $_SESSION['data'][$_SESSION['inputKlic']][$_SESSION['columnName'][3]] = clean_text($_POST['ulice']);
    $_SESSION['data'][$_SESSION['inputKlic']][$_SESSION['columnName'][4]] = clean_text($_POST['obec']);
    $_SESSION['data'][$_SESSION['inputKlic']][$_SESSION['columnName'][5]] = clean_text($_POST['PSC']);
    $_SESSION['data'][$_SESSION['inputKlic']][$_SESSION['columnName'][6]] = clean_text($_POST['telefon']);
    $_SESSION['data'][$_SESSION['inputKlic']][$_SESSION['columnName'][7]] = clean_text($_POST['email']);
    $_SESSION['data'][$_SESSION['inputKlic']][$_SESSION['columnName'][8]] = clean_text($_POST['zamestnani']);
    if ($_POST['nadrizenySelect'] !== '' && $_POST['nadrizenyInput'] === '') {
        $_SESSION['data'][$_SESSION['inputKlic']][$_SESSION['columnName'][9]] = clean_text($_POST['nadrizenySelect']);
    } else if ($_POST['nadrizenySelect'] === '' && $_POST['nadrizenyInput'] !== '') {
        $_SESSION['data'][$_SESSION['inputKlic']][$_SESSION['columnName'][9]] = clean_text($_POST['nadrizenyInput']);
    } else {
        $_SESSION['data'][$_SESSION['inputKlic']][$_SESSION['columnName'][9]] = '';
    }

    checkFile();
}

function checkFile(): void
{
    global $filename;

    if (file_exists($filename)) {
        if (is_writable($filename)) {
            writeFile($filename);
        } else {
            writeFile('novy'.$filename);
        }
    } else {
        writeFile('novy'.$filename);
    }
}

function writeFile($filename): void
{
    $file = fopen($filename, 'w');
    fputcsv($file, $_SESSION['columnName'], ';');
    foreach ($_SESSION['data'] as $el => $radek) {
        fputcsv($file, $radek, ';');
    }
    fclose($file);
}

#[NoReturn] function updateSuccessful(): void
{
    session_unset();
    header('Location: csvReadDU.php');
    exit();
}

#[NoReturn] function updateUnsuccessful(): void
{
    header('Location: csvReadDU.php');
    exit();
}

function submitSessionUnset(): void
{
    unset($_SESSION['input']);
    unset($_SESSION['errors']);
}

function fillInput(): void
{
    $_SESSION['input']['jmeno'] = clean_text($_POST['jmeno']);
    $_SESSION['input']['prijmeni'] = clean_text($_POST['prijmeni']);
    $_SESSION['input']['pohlavi'] = clean_text($_POST['pohlavi']);
    $_SESSION['input']['ulice'] = clean_text($_POST['ulice']);
    $_SESSION['input']['obec'] = clean_text($_POST['obec']);
    $_SESSION['input']['PSC'] = clean_text($_POST['PSC']);
    $_SESSION['input']['telefon'] = clean_text($_POST['telefon']);
    $_SESSION['input']['email'] = clean_text($_POST['email']);
    $_SESSION['input']['zamestnani'] = clean_text($_POST['zamestnani']);
    $_SESSION['input']['nadrizenyInput'] = clean_text($_POST['nadrizenyInput']);
}
function checkInput(): void
{
    $_SESSION['input']['jmeno'] = clean_text($_POST['jmeno'] ?? '');
    if ($_SESSION['input']['jmeno'] === '') {
        $_SESSION['errors']['jmeno'] = 'Jmeno musi byt uvedeno.';
    }

    $_SESSION['input']['prijmeni'] = clean_text($_POST['prijmeni'] ?? '');
    if ($_SESSION['input']['prijmeni'] === '') {
        $_SESSION['errors']['prijmeni'] = 'Prijmeni musi byt uvedeno.';
    }

    $_SESSION['input']['ulice'] = clean_text($_POST['ulice'] ?? '');

    $_SESSION['input']['obec'] = clean_text($_POST['obec'] ?? '');
    if ($_SESSION['input']['obec'] === '') {
        $_SESSION['errors']['obec'] = 'Obec musi byt uvedeno.';
    }

    $_SESSION['input']['PSC'] = clean_text($_POST['PSC'] ?? '');
    if (!preg_match('/^\d{5}/', $_SESSION['input']['PSC'])) {
        $_SESSION['errors']['PSC'] = 'PSC musi byt peti mistne cislo bez mezer.';
    }

    $_SESSION['input']['telefon'] = clean_text($_POST['telefon'] ?? '');
    if (!preg_match('/^\d{9}/', $_SESSION['input']['telefon'])) {
        $_SESSION['errors']['telefon'] = 'Telefon musi byt deviti mistne cislo bez mezer.';
    }

    $_SESSION['input']['email'] = clean_text($_POST['email'] ?? '');
    if ($_SESSION['input']['email'] !== '' && !filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) {
        $_SESSION['errors']['email'] = 'Zadejte platny mail.';
    }

    $_SESSION['input']['zamestnani'] = clean_text($_POST['zamestnani'] ?? '');
    $_SESSION['input']['nadrizenyInput'] = clean_text($_POST['nadrizenyInput'] ?? '');
    if ($_SESSION['input']['zamestnani'] === 'mistr' && ($_POST['nadrizenySelect'] !== '' || $_POST['nadrizenyInput'] !== '')) {
        $_SESSION['errors']['nadrizeny'] = 'Zamestnance s titulem mistr nemuze mit prirazeneho nadrizeneho!';
    } else if ($_SESSION['input']['zamestnani'] === 'dělník' && ($_POST['nadrizenySelect'] === '' || $_POST['nadrizenyInput'] !== '')) {
        $_SESSION['errors']['nadrizeny'] = 'Zamestnany delnik musi mit prirazeneho nadrizeneho z volby nadrizenych!.';
    }
}
function clean_text($string): string
{
    $tmp = trim($string);
    $tmp = stripslashes($tmp);

    return $tmp;
}