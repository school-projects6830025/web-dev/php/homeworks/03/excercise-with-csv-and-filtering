<?php
session_start();

$collator = collator_create('cs-CZ');

if (!empty($_GET)) {
    if (isset($_GET['jmeno'])) uasort($_SESSION['data'], customSort('Jméno'));
    else if (isset($_GET['prijmeni'])) uasort($_SESSION['data'], customSort('Příjmení'));
    else if (isset($_GET['ulice'])) uasort($_SESSION['data'], customSort('Ulice'));
    else if (isset($_GET['nadrizeny'])) uasort($_SESSION['data'], customSort('Nadřízený'));
    else if (isset($_GET['reset'])) ksort($_SESSION['data']);

    header('Location: csvReadDU.php');
} else {
    echo "Code BUG: table filter else call.";
}

function customSort($field): Closure
{
    return function ($a, $b) use ($field) {
        global $collator;
        return collator_compare($collator, $a[$field], $b[$field]);
    };
}